<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Abror
  Date: 09-Feb-22
  Time: 11:31 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@include file="header.jsp" %>--%>
<html>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Courses</title>
  <%--  <style><%@include file="/WEB-INF/views/bar.css"%></style>--%>
</head>


<h1>${message}</h1>
<div class="container" style="padding-top: 2rem">
</div>
<div class="container d-flex justify-content-around" style="padding-top: 1%;">
    <div class="row">
        <a href='/courses/add_books' class="btn btn-primary" style=
                "margin-left: 16px; float: left; margin-bottom: 2rem"
           onMouseOver="this.style.color='#0F0'"
           onMouseOut="this.style.color='#00F'">
            <i class="fas fa-plus"></i> Add new course </a>
        <br>

        <div class="col-md-12">
            <%--            ---------------------------------------------------------------------------------------------------------%>
                <div class="row">
                    <c:forEach items="${bookList}" var="course" varStatus="loop">

                        <div class="col-md-3 mt-3">

                            <div class="card" style="width: 18rem;">
                                <div class="card-body" >

                                    <br>
                                        <h5 class="card-title">${course.name}</h5>
                                    </a>
                                    <c:choose>
                                        <c:when test="${user.role.name().equals('ADMIN')||user.role.name().equals('MENTOR')}">
                                            <a class="btn btn-info" href='/courses/form?id=${course.id}'><i
                                                    class="fas fa-edit"></i>
                                            </a>
                                            <a class="btn btn-danger" href="/courses/delete/${course.id}"><i
                                                    class="fas fa-trash"></i> </a>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </div>

                        </div>
                        <%--            <c:set var="id" value="${task.userId}"/>--%>
                    </c:forEach>
                </div>

        </div>
    </div>
</div>
</body>
</html>
