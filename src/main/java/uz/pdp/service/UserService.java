package uz.pdp.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.dao.UserDao;
import uz.pdp.model.User;

import java.util.List;

@Service
public class UserService {


    @Autowired
    UserDao userDao;


    public User getUserByUsernamePassword(String username, String password) {
        return userDao.getUserByUsernamePassword(username, password);

    }




}
