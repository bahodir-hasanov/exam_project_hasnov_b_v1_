package uz.pdp.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.dao.BookDao;
import uz.pdp.model.Books;

import java.util.List;

@Service
public class BookService {


    @Autowired
    BookDao bookDao;


    @Transactional
    public Books getBooksById(int id) {
        return bookDao.getBooksById(id);
    }

    @Transactional
    public void saveBooks(Books books) {
        bookDao.saveBooks(new Books(books.getId(), books.getName()));
    }


    @Transactional
    public void deleteBooksById(int theId) {
        bookDao.deleteBooks(theId);
    }

    @Transactional
    public List<Books> getBookList() {
        return bookDao.getBooks();
    }


}
