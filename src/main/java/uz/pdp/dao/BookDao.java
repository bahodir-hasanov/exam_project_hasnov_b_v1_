package uz.pdp.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import uz.pdp.model.Books;

import java.lang.reflect.Type;
import java.sql.Array;
import java.util.List;


@Component
@Repository
public class BookDao {

    @Autowired
    JdbcTemplate template;

    @Autowired
    SessionFactory sessionFactory;


    public void deleteBooks(int theId) {
        Session session = sessionFactory.getCurrentSession();
        Books books = session.byId(Books.class).load(theId);
        session.delete(books);
    }

    public Books getBooksById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Books books = session.get(Books.class, id);
        return books;
    }

    public void saveBooks(Books books) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(books);

    }

    public List<Books> getBooks() {
        String query = "select * from books";
        List<Books> list = template.query(query, (rs, row) -> {
            Books books = new Books();
            books.setId(rs.getInt(1));
            books.setName(rs.getString(2));
            return books;
        });
        return list;
    }

}

