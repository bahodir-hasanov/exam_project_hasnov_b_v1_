package uz.pdp.controller;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import uz.pdp.model.Books;
import uz.pdp.model.User;
import uz.pdp.service.BookService;
import java.util.List;


@Controller
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    BookService bookService;

    @GetMapping
    public String getAllCourses(Model model) {
        List<Books> bookList = bookService.getBookList();
        model.addAttribute("bookList", bookList);
        return "view-courses";


    }

    @GetMapping(path = "/delete/{id}")
    public String deleteCourseById(@PathVariable int id) {
        bookService.deleteBooksById(id);
        return "redirect:/courses";
    }

    @PostMapping("/form")
    public String saveLesson(@ModelAttribute("books") Books books
    ) {

        bookService.saveBooks(books);

        return "redirect:/courses";

    }

    @RequestMapping("/add_books")
    public String addBooks() {
        return "lesson-form";
    }

    @GetMapping(path = "/form")
    public String getLessonForm(Model model,
                                @RequestParam(name = "id", required = false,
                                        defaultValue = "0") int id) {
        if (id != 0) {
            Books books = bookService.getBooksById(id);
            model.addAttribute("books", books);
        }

        return "lesson-form";
    }


}
