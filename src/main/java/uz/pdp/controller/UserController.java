package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import uz.pdp.model.User;
import uz.pdp.service.BookService;
import uz.pdp.service.UserService;


import javax.servlet.http.HttpSession;


@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    BookService bookService;

    @Autowired
    CourseController controller;

    @RequestMapping(path = "/login")
    public String showLoginForm() {
        return "login";
    }


    @RequestMapping(path = "/register")
    public String showRegisterForm() {
        return "register";
    }





    @RequestMapping(path = "/users/login", method = RequestMethod.POST)
    public String authUser(User user, Model model, HttpSession session) {
        String password = user.getPassword();
        String username = user.getUsername();
        User userFromDb = userService.getUserByUsernamePassword(username, password);
        if (userFromDb != null) {
            session.setAttribute("user", userFromDb);
            model.addAttribute("msg", "Welcome " + userFromDb.getFullName());
            switch (userFromDb.getRole()) {
                case ADMIN:
                    return "redirect:/courses";

            }
//            return "redirect:/courses";
        }
        model.addAttribute("msg", "Username or password is incorrect!");
        return "/login";

    }


    @RequestMapping(path = "/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("user");
        return "redirect:login";
    }












}
